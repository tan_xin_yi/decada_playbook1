# About the Playbook

<u>**Using the Playbook**</u>

The playbook is written as sections that can be read independently covering topics including architecture, design, application development, operations and security. 

For readers who are familiar with DECADA, they may navigate to the topics directly from this site’s navigation menu.

For readers who are starting out to implement with DECADA, you can use these guided steps:
- [Planning and Provision](Dev_Con/Onboard.md)


