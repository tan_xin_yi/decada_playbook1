# DECADA Pricing

## Cost Model



| <div style="width:270px">Price Structure</div>         | Proposed Pricing                                                                  | 
| :-----------------------------------------    | :---------------------------------------------------------------------------------- |  
| **(1) One-Time Implementation Fee** | Implementation effort required (man-days) based on GovTech’s charge out rate. Deployment tasks include the following: 
|| <ul>1. Organization Unit and Account Creation</ul><ul><ul> a. Mandatory: Yes </ul></ul><ul><ul> b. Estimated Time: 0.5 weeks </ul></ul><ul><ul> c. Purpose: To create a logical partition within DECADA</ul><ul><ul>for intended agency</ul></ul>|
|                                     | <ul>2. Sensor Device Modelling</ul><ul><ul> a. Mandatory: Yes </ul></ul><ul><ul>b. Estimated Time: 1 weeks </ul></ul><ul><ul> c. Purpose: To define the device model (attributes, measure points, events handling and services) in DECADA. This step is required for all connected devices for the purpose of doing upstream data structure validation, device identifying, database creation and more.</ul></ul> | 
|                                     | <ul>3. Asset Tree Configuration</ul><ul><ul>Mandatory: Yes</ul></ul><ul><ul>Estimated Time: 0.5 week</ul></ul><ul><ul>Purpose: To define the asset tree model for the organization</ul></ul> |
|                                     | <ul>4. Streamset and Batch Processing Script Setup</ul><ul><ul>Mandatory: Yes</ul></ul><ul><ul>Estimated Time: ~ 1-3 week</ul></ul><ul><ul>Purpose: To configure and program the sh/jar scripts required to realize the taskflow need for data extraction, transformation and loading.</ul></ul>|
|                                     | <ul>5. Report Generation Setup</ul><ul><ul>Mandatory: No<ul></ul><ul><ul>Estimated Time: 0.5 week</ul></ul><ul><ul>Purpose: To setup the configuration for auto report generation.</ul></ul>|
|                                     | <ul>6. Alert Rule Configuration</ul><ul><ul>Mandatory: No</ul></ul><ul><ul>Estimated Time: 0.5 week</ul></ul><ul><ul>Purpose: To configure the alert rule engine and notification list.</ul></ul>          |
|                                     | <ul>7. Application Development</ul><ul><ul>Mandatory: No</ul></ul><ul><ul>Estimated Time: ~Depends on scope</ul></ul><ul><ul>Purpose: To develop custom applications which interfaces with DECADA's data repository. Note that this is required only if Agency users has special use cases which the inbuilt interfaces can't provide.</ul></ul>                |
|                                     | <ul>8. Training</ul><ul><ul>Mandatory: No</ul></ul><ul><ul>Estimated Time: ~Depends on scope</ul></ul><ul><ul>Purpose: To provide on-site training to users</ul></ul>|
|**(2) Monthly data processing fee**  | $XX/XX (include XX month data storage); With a minimum monthly data processing fee of $XX/XX |
|**Basis of calculation**             | Man-days * GovTech’s charge out rate |


## Basis of calculation
 
Example 1: 
*To include based on the calculation basis stated above

<u>Notes:</u> 
GovTech is also exploring to price DECADA based on no. of insights provided to the Agencies. 
Data collected will be purged after 6 months. Data collected are accessible to Agencies and Agencies can download these data to their local drive. 


**TO BE REMOVED**


Sample TABLE Codes

**TO BE REMOVED**
**TO BE REMOVED**
**TO BE REMOVED**
**TO BE REMOVED**

<table>
  <tbody>
    <tr>
      <th>Tables</th>
      <th align="center">Are</th>
      <th align="right">Cool</th>
    </tr>
    <tr>
      <td>col 3 is</td>
      <td align="center">right-aligned</td>
      <td align="right">$1600</td>
    </tr>
    <tr>
      <td>col 2 is</td>
      <td align="center">centered</td>
      <td align="right">$12</td>
    </tr>
    <tr>
      <td>zebra stripes</td>
      <td align="center">are neat</td>
      <td align="right">$1</td>
    </tr>
    <tr>
      <td>
        <ul>
          <li>item1</li>
          <li>item2</li>
        </ul>
      </td>
      <td align="center">See the list</td>
      <td align="right">from the first column</td>
    </tr>
  </tbody>
</table>