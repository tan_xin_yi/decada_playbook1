# Accessing DECADA 

## Console
DECADA Console is a web-based Graphic User-Interface (GUI) for you to interact with resources within DECADA. The following figure shows a configuration interface in the DECADA Console

<div align=center>
<img width="900" src="./images/Deconsole.png"/>
</div>

The GUI consists of 3 parts.

### Banner
<div align=left><img width="40" src="./images/Banner.png"/> Where you can click to show or hide the navigation tree.

### Navigation tree
Where you can access the entries to all functions of DECADA Cloud. You can customised your view of navigation tree by adding your most frequently userd functions into favourites for quick access by clicking on the "Pin" icon right to the function.

### Configuration panel
Where you perform all configuration tasks around your data and resources on EnOS cloud.


## Application Programmable Interfaces (APIs)

Through the provided APIs from DECADA, Developers can add, delete, modify and retrieve resources such as users, assets, and applications, to faciliate development of applications with a variety of authentication and authorization techniques to ensure access to the APIs.

To access the DECADA APIs, you'll need to register the application that would invoke the APIs on DECADA. The application can then access the APIs by presenting its application key and secret to be authenticated and authorised. For more information, see Getting Started with APIs. 

Exposes APIs for various of services such as connection, model, asset, asset tree, alert, data. Provides SDKs for mainstream programming languages (C / Java / Python / Node.js, …) 






