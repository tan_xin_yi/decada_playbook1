# HIGH LEVEL KEY FEATURE of DECADA


<div align=center>
<img width="900" src="./images/DECADASystemDiagram.png"/>
</div>

## System Diagram depicting the Key Features in DECADA

At the bottom 


<div align=center>
<img width="850" src="./images/decadaIotHub.png"/>
</div>


## Data Classification
DECADA Cloud Platform can process and store up to Restricted classification data

## Edge Gateway
Through the use of Edge Gateway, various sensors can be connected via various protocol, and subsequently various protocol such as MQTT, CoAp, HTTP to be connected from Edge Gateway to DECADA Cloud.

## APIs/SDK
DECADA Cloud Platform provides APIs/SDK for data ingress, data egress and platform management which can be used as a stack for building of IoT Applications such as Dashboard, Mobile Apps and Analytics purposes.

<div align=center>
<img width="850" src="./images/DECADA_OV_Diagram.png"/>
</div>

<div align=center>
<img width="850" src="./images/key_features.png"/>
</div>


