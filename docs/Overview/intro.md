# Introduction

DECADA stands for **DE**vice Management, **C**ontrol **A**nd **D**ata **A**cquisition Systems.

It is a Multi-Tenant Cloud Platform which forms the Device Management Layer of Smart Nation Sensor Platform. This offers an easy and powerful solution for various agencies to implement and manage their Internet Of Things (IoT) networks.

<div align=center>
<img width="900" src="./images/decada_ov.png"/>
</div>



<div align=center>
<img width="900" src="./images/Decada_home.png"/>
</div>
