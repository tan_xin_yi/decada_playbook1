# Technical Architecture of DECADA

## Emterprise Architecture
<div align=center>
<img width="850" src="./images/techarch.png"/>
</div>

## Backend Architecture
<div align=center>
<img width="850" src="./images/backendarch.png"/>
</div>

## Infrastructure Hosting
<div align=center>
<img width="850" src="./images/infrahost.png"/>
</div>

## Deployment Architechture
<div align=center>
<img width="850" src="./images/deployarch.png"/>
</div>


## Data Flow
<div align=center>
<img width="850" src="./images/dataflow.png"/>
</div>
<div align=center>
<img width="850" src="./images/dataflow2.png"/>
</div>


## Batch Processing
<div align=center>
<img width="850" src="./images/batchprocessing.png"/>
</div>





