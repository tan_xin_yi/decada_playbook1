<!-- docs/sidebar -->

- [**Home**](/#docs)
- [*Release History*](Release.md)
- [**About The Playbook**](AboutPlayBook.md)
- [**Introduction to DECADA**](Overview/intro.md)
     * [DECADA Overview](Overview/Overview.md)
     * [DECADA Pricing Model](Overview/Pricing.md)
     * [Shared Responsibility Model](Overview/Role.md)
     * [Terminalogy](Overview/EnOSGlossaries.md)
 - [Technical Architechture](Overview/HighNet.md)

- [**On-Boarding Devices**](Dev_Con/Lifecycle.md)
     * [Planning and Provisioning](Dev_Con/Onboard.md)
     * [Service Phase](Dev_Con/Service.md)
     * [Maintenance and Management](Dev_Con/Maintenance.md)
     * [Decommissioning](Dev_Con/Decommis.md)

- **On-Boarding Applications**
     * [Integration with WOG Smart Nation Sensors Platform](Dev_Con/WOG.md)
     * [Integration with 3rd Party Application](Dev_Con/3PApp.md)
- [**Security**](Dev_Con/security.md)
- [**DECADA SDK & API**](Dev_Con/SDK.md)
- [**Use Cases**](Dev_Con/usedcase.md)
- [**FAQ**](Dev_Con/FAQ.md)
- [**Appendix**](Dev_Con/Appendix.md)

Additional Info (not in Playbook)
- [Technical Architechture](Overview/HighNet.md)
- [High Level Key Features](Overview/Price.md)
- [Accessing Decada](Overview/AccessingEnOS.md)
- [Overview Details](Overview/Overview2.md)
- [Direct Device Connection](Dev_Con/Dev_DDC.md)
- [Edge Gateway](Dev_Con/Edge.md)
- [API Connection](Dev_Con/API.md)        

