# Decommissioning Phase

In the decommissioning phase, Agencies can:
- Disable and delete the digital twin of a device from the cloud.

## Removal of Related Devices from DECADA
<div align=center>
<img src="./images/decom1.png"/>
</div>
Deletion of Devices

- Revoke the device certificagte that was issued to the device.
<div align=center>
<img src="./images/decom2.png"/>
</div>
Revocation of Device Certificate

- Archive the data of the decommisisioned devices.
<div align=center>
<img src="./images/decom3.png"/>
</div>
Data Archiving


