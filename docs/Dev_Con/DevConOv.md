# Device Connection Overview

Various methods of connection:
1. Direct Device Connection to DECADA Cloud
2. Edge Gateway Onsite
3. Edge Gateway Remote
4. Connect through IoT hub via MQTT protocol - Device Provisioning Service (DPS)



<div align=center>
<img width="800" src="./images/techstack_system.png"/>
</div>

## Step No 1

- Get HTTP URL
- Get server address
- do that

## Step No 2
- Get 
- Publish

## Step No 3
- Do this
- Do that

## Step No 4
- Do This again
- Do That again