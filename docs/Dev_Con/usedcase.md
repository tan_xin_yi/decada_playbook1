# DECADA Use Cases


## Use Case 1: AirGuard Project

<div align=center>
<name=Device Management Dashboard>
<img width="900" length="550" src="./images/uc1.jpg"/>
</div>
Air Guard Project 1

The proposed project involves the development of an IoT Device Management Platform for the monitoring and actuation of data for NEA’s Toxic Gases Detection System. The Edge Gateway System shall be responsible for interfacing with an Airguard devices situated on-site and providing a secured interface for upstream/downstream communication with DECADA.  NEA can access sensor information via Internet through GCC where the security aspect is taken care by GovTech. 



<div align=center>
<name=Device Management Dashboard>
<img width="900" length="550" src="./images/uc2.png"/>
</div>
AirGuard Project - 2
 
The DECADA Cloud together with the SNSP Solutioning (i.e SDX) shall also interface with NEA system to provide monitoring and actuation features. It will also allow NEA to access sensor information via SG-WAN on GSIB Laptop.


## PUB Use Case 2: Solar Panel Project


<div align=center>
<name=Device Management Dashboard>
<img width="900" length="550" src="./images/uc3.png"/>
</div>
Solar Panel Project
 
The proposed project involves the deployment of 4,000 solar panels at Seletar and Bedok Reservoirs where DECADA will be used to provide device management and visualization. PUB can have customized data analytics, visualization dash boarding and application development as per use case
EMA/SP Power Meter Deployment

## EMA Power Meter Deployment 
<name=Device Management Dashboard>
<img width="900" length="550" src="./images/uc4.png"/>
</div> 
EMA Power Meter Deployment

Note to team:
To grant access to view EMA confluence page. 
To share existing API/Adapters(both edge and backend)?

##1 LaaP

##2 


# Quicklinks

#  DECADA - IOT Tech Stack 

https://www.developer.tech.gov.sg/technologies/sensor-platforms-and-internet-of-things/decada-iot-tech-stack#!


# DECADA Starter Kit

https://docs.developer.tech.gov.sg/docs/decada-starter-kit-guide/#/